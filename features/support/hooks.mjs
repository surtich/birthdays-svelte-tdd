import { Before, After } from '@cucumber/cucumber';

import { setDefaultTimeout } from '@cucumber/cucumber';

setDefaultTimeout(60 * 1000);

Before(async function () {
	await this.launchServer();
	await this.launchBrowser();
});

After(async function () {
	await this.killServer();
	this.closeBrowser();
});
